# spDE
# Minimal desktop environment for GNU/Linux.
# https://speedie.site/projects/spde.php
#
# Copyright (C) 2022-2023 speedie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

include options.mk

help:
	@echo "make install     Install ${NAME}."
	@echo "make uninstall   Uninstall ${NAME}."
	@echo "make dist        Create distro package for ${NAME}."

install:
	cp ${NAME} ${PREFIX}${DESTDIR}/bin
	chmod +x ${PREFIX}${DESTDIR}/bin/${NAME}

uninstall:
	rm -rf ~/.config/${NAME}
	rm -f ${PREFIX}${DESTDIR}/bin/${NAME}

dist:
	mkdir -p ${NAME}-${VER}
	cp ${NAME} LICENSE Makefile *.mk *.md ${NAME}-${VER}
	tar -cf ${NAME}-${VER}.tar ${NAME}-${VER}
	gzip ${NAME}-${VER}.tar
	rm -rf ${NAME}-${VER}.tar ${NAME}-${VER}
